import pytest
from pytest import raises
from unittest.mock import MagicMock

from ConsonantCounter import Consonants


class ConsonantCounterTest:

    @classmethod
    def setup_class(cls):
        print("\nSetting up module")

    @classmethod
    def teardown_class(cls):
        print("\nTearing down module")

    @pytest.fixture()
    def consonants(self):
        consonants = Consonants()
        return consonants

    def test_count_consonants_1(self, consonants):
        demo = "world"
        consonants.add_string(demo)
        assert consonants.ConsonantCounter() == 4

    @pytest.mark.parametrize("string, result", [("hello", 3), ("world", 4)])
    def test_count_multiple_consonants(self, consonants, string, result):
        consonants.add_string(string)
        assert consonants.ConsonantCounter() == result

    def test_count_consonants_exception(self, consonants):
        demo = 2
        with pytest.raises(Exception):
            consonants.add_string(demo)

    def test_return(self, consonants, monkeypatch):
        mock_file = MagicMock()
        mock_file.readline = MagicMock(return_value="abcde")
        mock_open = MagicMock(return_value=mock_file)
        monkeypatch.setattr("builtins.open", mock_open)
        result = consonants.readFromFile("user_input")
        mock_open.assert_called_once_with("user_input", "r")
        consonants.add_string(result)
        assert 3 == consonants.ConsonantCounter()

